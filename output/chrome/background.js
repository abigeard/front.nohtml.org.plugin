﻿// Send message to current tab
/*kango.browser.tabs.getCurrent(function (tab) {
	tab.dispatchMessage(message, data);
});
*/

var msg_verbose = true;
var fn_verbose = true;


kango.ui.browserButton.setPopup({
    url: 'popup/popup.html',
    height: 600, width: 450
});

kango.addMessageListener('save_element', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	saver.addHTMLItem(event.data);
});

kango.addMessageListener('create_folder', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	saver.addFolder(event.data, event.target);
});

kango.addMessageListener('delete_folder', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	saver.removeFolder(event.data, event.target);
});

kango.addMessageListener('edit_folder', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	saver.editFolder(event.data.oldname, event.data.newname, event.target);
});

var saver = (function(){
	return {
		folders: {},
		
		addHTMLItem: function(element){
			var data = kango.storage.getItem('nohtml_elements');
			console.log(element);
			if(data == null){
				data = [];
			}	
			data.push({html : element.html, css : element.css, parent : element.parent, id : element.id});
			console.log(data);
			kango.storage.setItem('nohtml_elements', data);
		},	
		
		addFolder: function(name, receipt){
			this.folders = kango.storage.getItem('nohtml_folders');
			if(this.folders == null){
				this.folders = {};
			}

			if(!this.folders.hasOwnProperty(name)){
				this.folders[name] = {items : []};
				kango.storage.setItem('nohtml_folders', this.folders);
				receipt.dispatchMessage('create_folder_success', name);
			}
			else{
				receipt.dispatchMessage('create_folder_failure', name);
			}
		},
		
		editFolder: function(oldname, newname, receipt){
			var copy = this.folders[oldname];
			this.folders[newname] = copy;
			delete this.folders[oldname];
			console.log(this.folders);
			kango.storage.setItem('nohtml_folders', this.folders);
		},
		
		removeFolder: function(name, receipt){
			this.folders = kango.storage.getItem('nohtml_folders');
			delete this.folders[name];
			kango.storage.setItem('nohtml_folders', this.folders);
			receipt.dispatchMessage('delete_folder_success', name);
		},
	
		callback: function (fn) {
            return function (el) { return function () { fn.apply(el, arguments); } }(this);
        }
	};

}());
