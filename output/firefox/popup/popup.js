var msg_verbose = true;
var fn_verbose = true;


KangoAPI.onReady(function () {
	kango.browser.tabs.getCurrent(function (tab) {
		tab.dispatchMessage('is_active', {});
	});
	
	
	$('#start_elements').click(function(){
		kango.ui.optionsPage.open();
	});
	
	$('#clear_elements').click(function(){
		kango.storage.clear();
	});
	
	
	
		
	kango.addMessageListener('is_active_reply', function (event) {
		if(msg_verbose){
			console.log(event);
		}
		if(event.data){
			$('#start_nohtml').text('Stop');
			$('#start_nohtml').click(function(){
				kango.browser.tabs.getCurrent(function (tab) {
					tab.dispatchMessage('stop_nohtml', {});
				});
			});
		}
		else{
			$('#start_nohtml').text('Start');
			$('#start_nohtml').click(function(){
				kango.browser.tabs.getCurrent(function (tab) {
					tab.dispatchMessage('start_nohtml', {});
				});
			});
		}
	});
});

