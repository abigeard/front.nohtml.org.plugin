var msg_verbose = true;
var fn_verbose = true;



var ui = (function ($) {  
    //UI API
    return {         
		topbar: false,
		sidebar: false,
		current_folder: false,
		folders: {},
		
		initTopbar: function(){
			this.topbar = $('<div class="nohtml nohtml_topbar"></div>');
			$(document.body).append(this.topbar);
		},
		
		pushNotification: function(text){
			if(!this.topbar){
				this.initTopbar();
			}
			
			var notification = $('<div class="nohtml nohtml_notification">'+text+'</div>');
			$(this.topbar).append(notification);
			
			var cb_timeout = this.callback(function(){
				$(notification).remove();
			});
			setTimeout(cb_timeout, 3000);
		},
		
		pushWarningPrompt: function(text, fn_confirm, fn_cancel){
			console.log('warning');
			if(!this.topbar){
				this.initTopbar();
			}
			
			var notification = $('<div class="nohtml nohtml_warning nohtml_notification">'+text+'</div>');
			var confirm = $('<input type="button" value="Yes"/>');
			var cancel = $('<input type="button" value="No"/>');
			
			confirm.click(fn_confirm);
			cancel.click(fn_cancel);
			var cb_delete = this.callback(function(){
				notification.remove()
			});
			cancel.click(cb_delete);
			confirm.click(cb_delete);
			notification.append(confirm);
			notification.append(cancel);
			
			$(this.topbar).append(notification);
		},
		
		pushErrorNotification: function(text, fn_confirm, fn_cancel){
			if(!this.topbar){
				this.initTopbar();
			}
			
			var notification = $('<div class="nohtml nohtml_warning nohtml_notification">'+text+'</div>');
			$(this.topbar).append(notification);
			
			var cb_timeout = this.callback(function(){
				$(notification).remove();
			});
			setTimeout(cb_timeout, 3000);
		},
		
		
		
		initSidebar: function(){
		
			this.sidebar = $('<div class="nohtml nohtml_sidebar"></div>');
			var menu = $('<span class="nohtml nohtml_sidebar_side_menu"></span>');
			var toggler = $('<span class="nohtml nohtml_sidebar_toggler"><</span>');
			var adder = $('<span class="nohtml nohtml_sidebar_adder">+</span>');
			
			
			var cb_toggle = this.callback(function(){
				if(this.sidebar.css('right') == '0px'){
					this.hideSidebar();
					toggler.text('<');
				}
				else{
					this.showSidebar();
					toggler.text('>');
				}
			});
			toggler.click(cb_toggle);
			
			var cb_add = this.callback(function(){
				if(this.sidebar.css('right') != '0px'){
					this.showSidebar();
					toggler.text('>');
				}
				this.createNewFolder('', []);
			});
			adder.click(cb_add);
			
			
			menu.append(adder);
			menu.append(toggler);
			this.sidebar.append(menu);

			//$(this.sidebar).droppable({hoverClass: "nohtml_sidebar_hover", revert: "invalid", tolerance: "touch", drop : this.callback(this.handleSidebarDrop)});
			$(document.body).append(this.sidebar);
			
			this.loadFolders();
			this.loadUIFolders();
		},
		
		loadUIFolders: function(){
			var menu = $('<div class="nohtml nohtml_sidebar_menu"></div>');
			var trash = $('<div class="nohtml nohtml_sidebar_menu_item nohtml_trash"></div>');
			var adder = $('<div class="nohtml nohtml_sidebar_menu_item nohtml_add_folder"></div>');
			
			
			$(trash).droppable({hoverClass: "nohtml_trash_hover", tolerance: "touch", drop : this.callback(this.handleTrashDrop)});
			$(adder).droppable({hoverClass: "nohtml_add_folder_hover", tolerance: "touch", drop : this.callback(this.handleNewFolderDrop)});
			
			menu.append(trash);
			menu.append(adder);
			this.sidebar.append(menu);
			
		},
		
		handleTrashDrop: function(event, jui){
			this.pushWarningPrompt('Are you sure you want to delete this item', this.callback(function(){
				$(jui.draggable).remove();
			}),
			this.callback(function(){
				$(jui.draggable).animate({top : '50px', left : '50px'}); 
			}));
		},
		
		handleNewFolderDrop: function(event, jui){
			var folder = this.createNewFolder('', []);
			$(jui.draggable).addClass('nohtml_dropped');
			folder.find('.nohtml_folder').append(jui.draggable);
			$(jui.draggable).draggable('destroy');
		},
		
		loadFolders: function(){
			var cb_data = this.callback( function(folders){		
				if(folders != null && Object.keys(folders).length){
					this.folders = folders;
					
					for (var name in folders) {
						if (folders.hasOwnProperty(name)) {
							console.log(name);
							this.createNewFolder(name, folders[name].items);
						}
					}
				}
			});
			kango.invokeAsync('kango.storage.getItem', 'nohtml_folders', cb_data);
		
		},
		
		createNewFolder: function(name, items){
			var new_folder = false;
			if(name == ''){
				new_folder = true;
				console.log(Object.keys(this.folders).length);
				if(Object.keys(this.folders).length > 0){
					name = 'New Folder '+Object.keys(this.folders).length;
				}
				else{
					name = 'New Folder';
				}
			}
		
			var folder_group = $('<div class="nohmtl nohtml_folder_container"></div>');
			var folder = $('<div class="nohmtl nohtml_folder"></div>');
			var input_container = $('<span class="nohtml nohtml_folder_input_group"></span>');
			var input = $('<input type="text" class="nohtml nohtml_folder_input" maxlength="15" value="'+name+'"/>');
			var confirm = $('<input type="button" class="nohtml nohtml_folder_confirm" value="&#10004;"/>');
			
			var folder_menu = $('<span class="nohtml_folder_menu nohtml"></span>');
			var delete_button = $('<span class="nohtml nohtml_folder_menu_item nohtml_folder_delete">X</span>');
			var edit_button = $('<span class="nohtml nohtml_folder_menu_item">E</span>');
			var expand_button = $('<span class="nohtml nohtml_folder_menu_item">S</span>');
			
			
			
			var folder_name = $('<div class="nohtml nohtml_folder_name"></div>');
			folder_name.html($(input).val());
			folder_name.hide();
			
			cb_save = this.callback(function(){
				input_container.hide();
				var old_name = folder_name.html();

				folder_name.show();
				folder_name.html($(input).val());
				if(new_folder){
					this.saveFolder($(input).val());
				}
				else{
					this.editFolder(old_name, $(input).val());
				}
			});
			confirm.click(cb_save);
			
			var cb_edit = this.callback(function(){
				this.current_folder = folder_group;
				input_container.show();
				input.focus();
				folder_name.hide();
			});
			edit_button.click(cb_edit);
			
			var cb_view = this.callback(function(){
				this.current_folder = folder_group;
				if(folder.hasClass('nohtml_folder_expanded')){
					folder_group.removeClass('nohtml_folder_container_expanded');
					folder.removeClass('nohtml_folder_expanded');
				}
				else{
					folder_group.addClass('nohtml_folder_container_expanded');
					folder.addClass('nohtml_folder_expanded');
				}
			});
			expand_button.click(cb_view);
			
			var cb_delete = this.callback(function(){
				this.pushWarningPrompt('This will delete the folder and all its elements. Are you sure?', this.callback(function(){
					this.deleteFolder($(input).val(), folder_group);
				}), function(){});
			});
			delete_button.click(cb_delete);
			
			var cb_keyup = this.callback(function(event){
				if(event.keyCode == '13'){
					confirm.trigger('click');
				}
			});
			input.keyup(cb_keyup);
			
			
			
			folder.hover(this.callback(function(){
				folder_menu.css('top', '0');
			}),
			this.callback(function(){
				folder_menu.css('top', '-25%');
			}));

			
			folder_menu.append(delete_button);
			folder_menu.append(edit_button);
			folder_menu.append(expand_button);
			folder.append(folder_menu);
			
			input_container.append(input);
			input_container.append(confirm);

			folder_group.append(folder);
			folder_group.append(input_container);
			folder_group.append(folder_name);

			if(new_folder == false){
				input_container.hide();
				folder_name.show();
			}
						
			this.sidebar.append(folder_group);
			folder.droppable({hoverClass: "nohtml_folder_hover", revert: "invalid", tolerance: "touch", drop : this.callback(this.handleFolderDrop)});
			
			var cb_blur = this.callback(function(){
				confirm.trigger('click');
			});
			//input.blur(cb_blur);
			input.focus();
			
			this.current_folder = folder_group;
			return folder_group;
		},
		
		
		deleteFolder: function(name, folder){
			folder.remove();
			kango.dispatchMessage('delete_folder', name);
		},
		
		handleFolderDrop: function(event, jui){		
			$(jui.draggable).addClass('nohtml_dropped');
			$(jui.draggable).draggable('destroy')
			$(event.target).append(jui.draggable);
			
			this.hideSidebar();
			// Add to folder on background
		},
		
		saveFolder: function(name){
			// Saving new folder
			this.folders[name] = {items : []};
			kango.dispatchMessage('create_folder', name);
		},
		
		editFolder: function(old_name, new_name){
			// Edit
			var copy = this.folders[old_name];
			this.folders[new_name] = copy;
			delete this.folders[old_name];
			kango.dispatchMessage('edit_folder', {oldname : old_name, newname : new_name});

		},
		
	/*	handleSidebarDrop: function(event, jui){
			console.log(event);
			console.log(jui);
			
			var newfolder = this.createNewFolder('', []);
			$(jui.draggable).addClass('nohtml_dropped');
			$(jui.draggable).draggable('destroy')
			$(newfolder).find('.nohtml_folder').append(jui.draggable);
			
		},
		*/
		
		showSidebar: function(){
			if(!this.sidebar){
				this.initSidebar();
			}
			
			this.sidebar.css('right', '0px');
		
		},
		
		
		hideSidebar: function(){
			if(!this.sidebar){
				this.initSidebar();
			}
			
			this.sidebar.css('right', '-98%');
		
		},
		
		// Enables accessing the UI object as this under any callback put through this function.
		callback: function (fn) {
            return function (el) { return function () { fn.apply(el, arguments); } }(this);
        }
    };
}(jQuery || false));
