// ==UserScript==
// @name nohtml_content
// @all-frames false
// @run-at document-start
// @include http://*
// @include https://*
// @require ../libraries/jquery-2.1.1.min.js
// @require ../ui/ui.js
// ==/UserScript==


var msg_verbose = true;
var fn_verbose = true;

console.log(ui);

kango.invokeAsync('kango.io.getExtensionFileContents', 'extension/extension.css', function(content) {
        $(document).find('head').append('<style type="text/css">'+content+'</style>');
});

var nohtml = (function ($) {  
    //noHTML API
    return {         
		overlay: false,
		target: false,
		id: 1,
		
		start: function(){
			this.setOverlay();

			// Disables default right click context menu
			$(document).on('contextmenu', function(){
				return false;
			});
		
			var cb_hover = this.callback(function(event){		
				if(this.target){
					$(this.target).unbind('mousedown');
				}
				
				if(!$(event.target).hasClass('nohtml')){
					var cb_mousedown = this.callback(function(event){		
						if(event.which == 3 && event.target == event.data.target){
							// Show menu
							this.showMenu(event.data.target, event);
						}
						else if((event.which == 1 || event.which == 2) && !$(event.target).hasClass('nohtml_menu')){
							$('.nohtml_menu_container').remove();
						}
					});
					$(event.target).mousedown({
						target: this.target
					}, cb_mousedown);
					this.target = event.toElement;
					
					
					$(this.overlay).css('width', $(event.toElement).outerWidth()-1);
					$(this.overlay).css('height', $(event.toElement).outerHeight()-1);
					$(this.overlay).css('top', $(event.toElement).offset().top - $(document).scrollTop());
					$(this.overlay).css('left', $(event.toElement).offset().left- $(document).scrollLeft());
				}
			});
	
			$(document.body).mousemove(cb_hover);
				
			
			$(document.body).append(this.overlay);
		},
		
		stop: function(){
			$(this.overlay).remove();
			$(this.target).unbind('mousedown');
			
			this.overlay = false;
			this.target = false;
			
			$(document.body).unbind('mousemove');
		},
		
		setOverlay: function(){
			this.overlay = $('<span class="nohtml_plugin nohtml_overlay"></span>');
		},
		
		setScrollResponsiveness: function(){
			var cb_top = this.callback(function(event){
				if(this.overlay){
					$(this.overlay).css('top', $(this.overlay).offset().top - $(document).scrollTop());
					$(this.overlay).css('left', $(this.overlay).offset().left - $(document).scrollLeft());
				}
			});
			
		},
		
		
		// Pops up the right click menu for a selected element
		showMenu: function(element, event){
			// Cloning the HTML element so that modifications don't affect the original
			var clone = $(element).clone();
			$('.nohtml_menu_container').remove();
			var container = $('<div class="nohtml_plugin nohtml_menu nohtml_menu_container"></div>');
			var menu = $('<ul class="nohmtl_list"></ul>');
			
			
			
			
			var cb_cancel = this.callback(function(){
				container.remove();
			});
			var cancel = $('<li class="nohtml_plugin nohtml_list_item nohtml_menu">Cancel</li>').click(cb_cancel);
			
			var cb_save_all = this.callback(function(){
				this.sanitizeAndSend(element);		
				
				container.remove();
			});
			var save_all = $('<li class="nohtml_plugin nohtml_list_item nohtml_menu">Save element and content</li>').click(cb_save_all);
			
			var cb_save = this.callback(function(){
				element.innerHTML = '';
				this.sanitizeAndSend(element);
				container.remove();
			});
			var save_container = $('<li class="nohtml_plugin nohtml_list_item nohtml_menu">Save element</li>').click(cb_save);
			
			menu.append(save_all);
			menu.append(save_container);
			menu.append(cancel);
			container.append(menu);
			
			container.css('left', event.screenX);
			container.css('top', event.screenY - 80);
			
			$(document.body).append(container);
		},

		
		sanitizeAndSend: function(element){
			if(!element.nodeId){
				element.nodeId = 0;
				element.parentId = -1;
			}
			var clone = $(element).clone();
				
			var attributes = $.map($(clone)[0].attributes, function(item) {
				return item.name;
			});

			$.each(attributes, function(i, item) {
				$(clone).removeAttr(item);
			});
			
			// Pushing parent element's CSS first to keep the elements in a consistent order.
			var css = [];
			css.push(this.sanitizeCSS(window.getComputedStyle(element)));
			

			var cb_find = this.callback(function(index, child){
				css.push(this.sanitizeCSS(window.getComputedStyle(child)));
				
				/*var attributes = $.map($(child)[0].attributes, function(item) {
					return item.name;
				});
				
				$.each(attributes, function(i, item) {
					$(child).removeAttr(item);
				});*/
			});
			$(element).find('*').each(cb_find);

			kango.dispatchMessage('save_element', {html : $(clone)[0].outerHTML, css : css , parent : element.parentId, id : element.nodeId});
			ui.pushNotification('Your shizzles been added');
		},
		
		// Takes a CSSStyleDeclaration object, returns the same object purged of every defaulted values
		sanitizeCSS: function(css){
			/*var newCss = {};
			for(var i = css.length-1; i >= 0; i--) {
				var name = css[i];
				var value = css.getPropertyValue(name);
			//	if(value != '0px' && value != '0' && value != 'visible' && value != 'normal' && value != 'auto' && value != 'none'){
					newCss[name] = value;
			//	}
			}*/
			return css;
			
		
		},
		
		
		// Enables accessing the nohtml object as this under any callback put through this function.
		callback: function (fn) {
            return function (el) { return function () { fn.apply(el, arguments); } }(this);
        }
    };
}(jQuery || false));

kango.addMessageListener('start_nohtml', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	nohtml.start();
});

kango.addMessageListener('stop_nohtml', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	nohtml.stop();
});

kango.addMessageListener('is_active', function (event) {
	if(msg_verbose){
		console.log(event);
	}
	event.target.dispatchMessage('is_active_reply', (nohtml.overlay != false));
});