KangoAPI.onReady(function () {
	
	var loader = (function(){
		return {
			init: function(){
				ui.hideSidebar();
				$(document.body).droppable();
				this.load();
			
			},
		
			load: function(){
				var boxes = [];
				var data = kango.storage.getItem('nohtml_elements');
				if(data != null){
					var cb_data = this.callback(function(elem, index){
						var box = $(elem.html);
							box.css(elem.css[0]);

						if(Array.isArray(elem.css)){
							console.log(2);
							var cb_each = this.callback(function(index, child){
								console.log(elem.css[index+1]);
								$(child).css(elem.css[index+1]);
							});
							$(box).find('*').each(cb_each);
						}
						
						$(box).draggable({revert : "invalid", start : this.callback(this.dragStart), stop : this.callback(this.dragStop), addClasses : "nohtml_dragged", cursorAt: { left: 0, top : 0 }});
							
						$(document.body).append(box);
					});
					data.forEach(cb_data);
				}
			},	
			
			// ui named jui to avoid confusion with the nohtml UI. derp
			dragStart: function(event, jui){
			   $(event.target).addClass('nohtml_dragged');
			   console.log(event.pageY);
			   $(event.target).removeClass('nohtml_dropped');
			   
			   console.log(event);
				ui.showSidebar();
			},
			
			dragStop: function(event, jui){
				$(event.target).removeClass('nohtml_dragged');
			},
		
			callback: function (fn) {
				return function (el) { return function () { fn.apply(el, arguments); } }(this);
			}
		};

		
		
		
	}());
	
	
	kango.addMessageListener('create_folder_success', function (event) {
		if(msg_verbose){
			console.log(event);
		}
		// nothing for now
	});
	
	kango.addMessageListener('create_folder_failure', function (event) {
		if(msg_verbose){
			console.log(event);
		}
		ui.pushErrorNotification('The folder '+event.data+' already exists, please choose another name');
		ui.current_folder.find('.nohtml_folder_input_group').show();
		ui.current_folder.find('.nohtml_folder_input').focus();
		ui.current_folder.find('.nohtml_folder_name').hide();
	});
	
	loader.init();
});

